#### A brief description

This repo hosts my solution to an interview assignment.
Its contents have a pure demonstrational purpose.

The original requirements are found in `specs/Requirements.txt`

The core library `LibRecords` contains a proof-of-concept
implementation of a generic parser for structured data records,
and it could _probably_ bear some kind of usefulness for
real-life scenarios.
Contents marked as `Copyright (c) 2021 Filippo Bergamo`
are *free software*, released under the terms of GPL v3, so
anybody is welcome to take what's needed and expand on my code.

Below is a brief explanation of the key implementation choices
and other non-obvious details, as required by the original assignment.


#### Notes on the project format

* The project is written in C#, using .Net standard 2.0,
  .Net Framework 4.6.1 and .Net core 2.1
* The solution contains 6 projects:
   - `App`
   - `LibFootballStandings`
   - `LibRecords`
   - `LibTextify`
   - `LibWeatherRecords`
   - `Textify`
* The main program is defined in project `App`
  and is composed of a simple WinForms application
  that represents the user interface.
* The core application logic is implemented in libraries
  `LibRecords`, `LibWeatherRecords` e `LibFootballStandings`.


#### LibRecords - Description

`LibRecords` is the project's core; it exposes a generic data
structure named `DataRecords`, that represents a set of
`Record` objects;

Each `Record` represents a single tuple, made of a sequence
of `Field` objects, each paired with a `FieldInfo` instance
containing all parameters that are needed to parse a line
of text into the corresponding `Field` object, together with
information about the field content's type.

The constructor of `DataRecords` requires an array of `FieldInfo`,
representing the structure of the tuples that will fill the
current instance (type and order of each `Field`).

Data deserialization happens by invoking method `Import` on a
`DataRecords` instance, passing in all lines of a `.dat` file.


#### LibWeatherRecords, LibFootballStandings

Libraries `LibWeatherRecords` and `LibFootballStandings` each
implement a specialized version of `DataRecord`, representing
the specific data format of files `weather.dat` and `football.dat`.
The specific list of `FieldInfo` for both formats is declared in
`DataFormat`. The structure of the tuples is defined by the
array `DataFormat.AllFields`.


#### Running the application

By running `App.exe`, the user interacts with a GUI presenting 2 
buttons and a read-only textbox.

Pressing the left button starts the import procedure for file
`weather.dat` and triggers the data analysis procedure to
extract what is required by the given specifications.
(i.e. the day with the smallest temperature variation).

Similarly, pressing the right button starts the procedure
for football league data.

Any format errors in the input data or any exceptions encountered
during the import procedure are handled gracefully and reported
in the read-only textbox and also via modal-window messages.


#### Implementation choices - weather.dat

The following implementation choices were deemed necessary
by analyzing the data format of file `weather.dat`:

- The presence of an asterisk next to some of the values
  was handled by creating the specialized data type
  `MarkedValue` and assigning it to fields "MxT", "MnT"
  and "MxS"; this data type allows to hold both a decimal
  value and a Boolean flag that signals the presence of
  an asterisk besides the value.

- The absence of data in correspondence of some of the columns
  in some tuples was handled by marking the corresponding fields
  as "optional" with the parameter `FieldInfo.IsOptional`.
  This avoids import errors in cases where the target field
  contains no data.

- Similarly, the presence of a last "summary" line containing
  less fields than the other lines was handled by making 
  some fields optional. Furthermore, it is possible to chose
  whether to parse or to skip such summary line, via the option
  `Weather_SkipSummary` in the application configuration file
  (`App.config`).


#### Implementation choices - football.dat

The following implementation choices were deemed necessary
by analyzing the data format of file `football.dat`:

- It is possible to handle the presence of a "separator" line
  between records #17 and #18, either as a desired situation
  or as a data format anomaly.
  If such "separator" line is to be considered legit, the option
  `Football_AllowSeparatorLine` can be set to `true`;
  conversely, if such a line is to be considered a data format
  error, the option can be set to `false`.

In general, a specific behaviour for each data line can be
implemented by overriding method `DataRecords.ShouldParseLine()`;
if the method returns `false` the current line is ignored by
the parsing logic.

In the same way, it's also possible to handle the presence of one
or more heading lines before the actual records, as done for
example in `WeatherRecords`:
```
if (lineNumber == 0
    || lineNumber == 1)
    return false; //skip first two header lines
```


#### Data extraction

The class `Analysis<TRecord, TResult>` implements generic
behaviours for deserialization, error handling and information
extraction.

The specialized extraction logic is modeled on the `reduce`
paradigm, implemented by `DataReports.Reduce<T>()` and by
delegate `ReduceFunc<T>`, for which a specialized implementation
is provided in methods `WeatherAnalysis.Reduce()`
and `FooballAnalysis.Reduce()`.


#### Extensibility

The project was conceived to be flexible and extensible.
Specialized behaviours for data sets with different formats
can be obtained simply by deriving from the generic classes
provided in `LibRecords`.
Further extensions are achievable via specialized lambdas
given as input to the `Analysis` constructor.
This allows to adapt the data-parsing and information-extracting
logic to the specific input data format while leaving the general
application-level logic untouched.


#### Extensibility of the User Interface

The UI implementation was intentionally kept very simple for
a pure demonstrational purpose.
A "weak" coupling between the user-interaction level and
the parsing- and analysis- logic is guaranteed by interface
`IFrontend`, that serves as an abstraction layer allowing
to swap in different UI implementations while keeping the
application logic unmodified.
