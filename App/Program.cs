﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Windows.Forms;
using LibRecords;
using LibWeatherRecords;
using LibFootballStandings;


namespace App
{
    static class Program
    {
        private static GuiMain Gui;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Gui = new GuiMain();
            Gui.RunFootball += OnGuiRunFootball;
            Gui.RunWeather += OnGuiRunWeather;

            Application.Run(Gui);
        }

        private static void OnGuiRunWeather(object sender, EventArgs e)
        {
            var a = new WeatherAnalysis(
                Gui,
                Config.Weather_SkipSummary);

            ShowResult(a);
        }

        private static void OnGuiRunFootball(object sender, EventArgs e)
        {
            var a = new FooballAnalysis(
                Gui,
                Config.Football_AllowSeparatorLine);

            ShowResult(a);
        }

        private static void ShowResult<T, R>(Analysis<T, R> a)
            where T: DataRecords
        {
            var result = a.Run();

            if (result is null)
            {
                Gui.ShowError(
                    "The analysis failed to produce results!");
            }
            else
            {
                var msg =
                    "The analysis' result record is: \n" +
                    $"Record #{result.RecordIndex}, {result.RecordName} \n" +
                    $"{result.ResultName}: {result.ResultValue}";

                Gui.ShowMessage(msg);
            }
        }

    }
}
