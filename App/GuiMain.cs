﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibRecords;

namespace App
{
    public partial class GuiMain
        : Form,
        IFrontend
    {
        public event EventHandler RunFootball;

        public event EventHandler RunWeather;

        public GuiMain()
        {
            InitializeComponent();

            BtnFootball.Click +=
                (s, e) => RunFootball?.Invoke(this, new EventArgs());

            BtnWeather.Click +=
                (s, e) => RunWeather?.Invoke(this, new EventArgs());
        }

        public void WriteLog(string msg)
        {
            this.TxbLog.AppendText($"{msg}\n");
        }

        public void ShowMessage(string msg)
        {
            MessageBox.Show(msg);
        }

        public void ShowError(string msg)
        {
            MessageBox.Show(
                msg,
                "ERROR",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }

        public void Log(string msg)
        {
            this.WriteLog(msg);
        }

        public string[] GetRecordLines()
        {
            string filePath = null;
            using (var b = new OpenFileDialog())
            {
                b.Title = "Please, select a file";
                b.Filter = "data files (*.dat)|*.dat";
                b.Multiselect = false;

                if (b.ShowDialog() != DialogResult.OK)
                {
                    ShowError(
                        "Cannot continue without selecting a file!");
                    return null;
                }

                filePath = b.FileName;
            }

            return File.ReadAllLines(filePath);
        }
    }
}
