﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    static class Config
    {
        public static bool Weather_SkipSummary
            => ReadBool("Weather_SkipSummary", false);

        public static bool Football_AllowSeparatorLine
            => ReadBool("Football_AllowSeparatorLine", true);

        public static bool ReadBool(string key, bool defval)
        {
            var strval = ConfigurationManager.AppSettings.Get(key);
            if (string.IsNullOrEmpty(strval))
                return defval;

            if (bool.TryParse(strval, out bool b))
                return b;
            else
                return defval;
        }
    }
}
