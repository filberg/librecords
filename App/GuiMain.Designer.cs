﻿
namespace App
{
    partial class GuiMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnWeather = new System.Windows.Forms.Button();
            this.TxbLog = new System.Windows.Forms.RichTextBox();
            this.BtnFootball = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnWeather
            // 
            this.BtnWeather.Location = new System.Drawing.Point(31, 28);
            this.BtnWeather.Name = "BtnWeather";
            this.BtnWeather.Size = new System.Drawing.Size(253, 48);
            this.BtnWeather.TabIndex = 0;
            this.BtnWeather.Text = "Find largest daily temp. variation";
            this.BtnWeather.UseVisualStyleBackColor = true;
            // 
            // TxbLog
            // 
            this.TxbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxbLog.Location = new System.Drawing.Point(34, 170);
            this.TxbLog.Name = "TxbLog";
            this.TxbLog.ReadOnly = true;
            this.TxbLog.Size = new System.Drawing.Size(811, 291);
            this.TxbLog.TabIndex = 1;
            this.TxbLog.Text = "";
            // 
            // BtnFootball
            // 
            this.BtnFootball.Location = new System.Drawing.Point(57, 28);
            this.BtnFootball.Name = "BtnFootball";
            this.BtnFootball.Size = new System.Drawing.Size(253, 48);
            this.BtnFootball.TabIndex = 2;
            this.BtnFootball.Text = "Find best goal difference";
            this.BtnFootball.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BtnWeather);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(34, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 105);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Weather Data";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BtnFootball);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(488, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(357, 105);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Football Standings";
            // 
            // GuiMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 484);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TxbLog);
            this.Name = "GuiMain";
            this.Text = "Weather and Football Data";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnWeather;
        private System.Windows.Forms.RichTextBox TxbLog;
        private System.Windows.Forms.Button BtnFootball;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

