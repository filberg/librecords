﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LibTextify
{
    public class TextifiedInfo
    {
        public string RelativePath { get; }

        private StreamReader Reader { get; set; }

        private TextifiedInfo(
            string relativePath,
            StreamReader reader)
        {
            this.RelativePath = relativePath;
            this.Reader = reader;
        }

        public void Restore(
            DirectoryInfo rootDir)
        {
            if (Reader is null)
                throw new InvalidOperationException(
                    "Cannot restore in the current state");

            var urid = new Uri(rootDir.FullName);
            var uri = new Uri(urid, RelativePath);

            var p = uri.LocalPath;

            var dirpath = Path.GetDirectoryName(p);
            if (!Directory.Exists(dirpath))
                Directory.CreateDirectory(dirpath);

            string line;

            try
            {
                using (var fw = new StreamWriter(p, false))
                {
                    while ((line = Reader.ReadLine()) != null)
                    {
                        if (line.StartsWith(Textifier.OpenBoundary))
                        {
                            line = Reader.ReadLine();

                            var clstag = Textifier.GetEndTag(RelativePath);

                            if (!line.StartsWith(clstag))
                                throw new Exception(
                                    "Failed to parse end tag");

                            line = Reader.ReadLine();
                            if (!line.StartsWith(Textifier.CloseBoundary))
                                throw new Exception(
                                    "Failed to parse close boundary");

                            // read end of file
                            return;
                        }

                        fw.WriteLine(line);
                    }
                }
            }
            finally
            {
                Reader = null;
            }
        }

        public static TextifiedInfo FindNext(
            StreamReader reader)
        {
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith(Textifier.OpenBoundary))
                {
                    var startTag = reader.ReadLine();

                    if (!startTag.StartsWith(Textifier.StartTagPrefix))
                        throw new Exception(
                            "Failed to parse start tag");

                    line = reader.ReadLine();
                    if (!line.StartsWith(Textifier.CloseBoundary))
                        throw new Exception(
                            "Failed to parse close boundary");

                    var idxStart = startTag.IndexOf('{');
                    var idxEnd = startTag.LastIndexOf('}');

                    var len = idxEnd - idxStart - 1;

                    if (idxStart < 0
                        || idxEnd < 0
                        || idxEnd < idxStart)
                        throw new Exception(
                            "Failed to parse relative path");

                    var relpath = startTag.Substring(
                        idxStart + 1,
                        len);

                    return new TextifiedInfo(relpath, reader);
                }
            }

            return null;
        }
    }
}
