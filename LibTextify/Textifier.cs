﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.IO;
using System.Text;

namespace LibTextify
{
    public class Textifier
    {
        public const string FSEPARATOR =
            "eu.filberg.LibTextify.Separator";

        public static string OpenBoundary { get; }
            = $"--{FSEPARATOR}";

        public static string CloseBoundary { get; }
            = $"{OpenBoundary}--";

        public static string EndTagPrefix { get; }
            = "//     END FILE";

        public static string StartTagPrefix { get; }
            = "//     BEGIN FILE ";

        public DirectoryInfo BaseDir { get; }

        public Func<FileSystemInfo, bool> ExcludeFn { get; }

        public Textifier(
            DirectoryInfo rootDir)
            : this(rootDir, null)
        {
        }

        public Textifier(
            DirectoryInfo rootDir,
            Func<FileSystemInfo, bool> excludeFn)
        {
            this.BaseDir = rootDir
                ?? throw new ArgumentNullException(
                    nameof(rootDir));

            if (!rootDir.Exists)
                throw new ArgumentException(
                    $"Invalid {rootDir}");

            this.ExcludeFn = excludeFn;
            if (excludeFn is null)
                this.ExcludeFn = (f) => false;
        }

        public void Textify(
            string outFile)
        {
            if (string.IsNullOrEmpty(outFile))
                throw new ArgumentNullException(
                    nameof(outFile));

            if (File.Exists(outFile))
                File.Delete(outFile);

            AddDir(BaseDir, outFile);
        }

        public void Untextify(
            string srcFile)
        {
            if (string.IsNullOrEmpty(srcFile))
                throw new ArgumentNullException(
                    nameof(srcFile));

            if (!File.Exists(srcFile))
                throw new ArgumentException(
                    $"Invalid nameof({srcFile})");

            if (!BaseDir.Exists)
                Directory.CreateDirectory(BaseDir.FullName);

            using (var fr = new StreamReader(srcFile, Encoding.UTF8))
            {
                TextifiedInfo info;
                while ((info = TextifiedInfo.FindNext(fr)) != null)
                    info.Restore(BaseDir);
            }
        }

        private void AddFile(
            FileInfo srcFile,
            string outFile)
        {
            using (var fr = new StreamReader(srcFile.FullName, Encoding.UTF8))
            using (var fw = new StreamWriter(outFile, true))
            {
                var hdr = GetHeaderFooterString(srcFile, true);
                fw.WriteLine(hdr);

                string line;
                while ((line = fr.ReadLine()) != null)
                    fw.WriteLine(line);

                var ftr = GetHeaderFooterString(srcFile, false);
                fw.WriteLine(ftr);
            }
        }

        private void AddDir(
            DirectoryInfo dirinfo,
            string outfile)
        {
            foreach (var f in dirinfo.EnumerateFiles())
            {
                if (!ExcludeFn(f))
                    AddFile(f, outfile);
            }

            foreach (var d in dirinfo.EnumerateDirectories())
                if (!ExcludeFn(d))
                    AddDir(d, outfile);
        }

        private string GetHeaderFooterString(
            FileInfo file,
            bool header)
        {
            var uriF = new Uri(file.FullName);
            var uriD = new Uri(BaseDir.FullName);
            var reluri = uriD.MakeRelativeUri(uriF);

            var str = $"\r\n{OpenBoundary}\r\n";

            if (header)
                str += GetStartTag(reluri.ToString());
            else
                str += GetEndTag(reluri.ToString());

            str += $"\r\n{CloseBoundary}\r\n\r\n";

            return str;
        }

        public static string GetStartTag(string reluri)
        {
            return $"{StartTagPrefix} {{{ reluri }}} ";
        }

        public static string GetEndTag(string reluri)
        {
            return $"{EndTagPrefix} {{{ reluri }}} ";
        }

    }
}
