/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using LibRecords;

namespace LibWeatherRecords
{
    public class MarkedValueFieldInfo
        : FieldInfo<MarkedValue>
    {
        public MarkedValueFieldInfo(
            string name,
            int startIdx,
            int endIdx,
            bool isOptional)
            : base(
                name,
                startIdx,
                endIdx,
                isOptional,
                (string v, out MarkedValue r) =>
                {
                    r = null;
                    int idxMark = v.IndexOf('*');

                    if (idxMark == 0)
                        // no value before '*', invalid data format
                        return false;

                    if (idxMark > 0)
                        v = v.Substring(0, idxMark);

                    if (decimal.TryParse(v.Trim(), out decimal d))
                    {
                        r = new MarkedValue(d, idxMark > 0);
                        return true;
                    }

                    return false;
                })
        {
        }
    }
}