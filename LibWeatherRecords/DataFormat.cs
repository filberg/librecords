﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using LibRecords;

namespace LibWeatherRecords
{
    public static class DataFormat
    {
        public static DayFieldInfo Day { get; }
            = new DayFieldInfo("Dy", 0, 3, false);

        public static MarkedValueFieldInfo MaxTemp { get; }
            = new MarkedValueFieldInfo("MxT", 5, 10, false);

        public static MarkedValueFieldInfo MinTemp { get; }
            = new MarkedValueFieldInfo("MnT", 11, 16, false);

        public static DecimalFieldInfo AverageTemp { get; }
            = new DecimalFieldInfo("AvT", 17, 22, false);

        public static DecimalFieldInfo HDDay { get; }
            = new DecimalFieldInfo("HDDay", 23, 27, true);

        public static DecimalFieldInfo AvDP { get; }
            = new DecimalFieldInfo("AvDP", 28, 34, false);

        public static StringFieldInfo HourlyP { get; }
            = new StringFieldInfo("1HrP", 35, 39, true);

        public static DecimalFieldInfo TPcpn { get; }
            = new DecimalFieldInfo("TPcpn", 40, 45, false);

        public static StringFieldInfo WxType { get; }
            = new StringFieldInfo("WxType", 46, 52, true);

        public static StringFieldInfo PDir { get; }
            = new StringFieldInfo("PDir", 53, 57, true);

        public static DecimalFieldInfo AvSp { get; }
            = new DecimalFieldInfo("AvSp", 58, 62, false);

        public static StringFieldInfo Dir { get; }
            = new StringFieldInfo("Dir", 63, 66, true);

        public static MarkedValueFieldInfo MxS { get; }
            = new MarkedValueFieldInfo("MxS", 67, 70, true);

        public static DecimalFieldInfo SkyC { get; }
            = new DecimalFieldInfo("SkyC", 71, 74, false);

        public static IntFieldInfo MxR { get; }
            = new IntFieldInfo("MxR", 75, 79, true);

        public static IntFieldInfo MnR { get; }
            = new IntFieldInfo("MnR", 80, 82, true  );

        public static DecimalFieldInfo AvSPL { get; }
            = new DecimalFieldInfo("AvSPL", 83, 88, true);

        public static FieldInfo[] AllFields { get; }
            = new FieldInfo[]
            {
                Day,
                MaxTemp,
                MinTemp,
                AverageTemp,
                HDDay,
                AvDP,
                HourlyP,
                TPcpn,
                WxType,
                PDir,
                AvSp,
                Dir,
                MxS,
                SkyC,
                MxR,
                MnR,
                AvSPL
            };
    }
}
