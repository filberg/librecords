/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using LibRecords;

namespace LibWeatherRecords
{
    public class WeatherRecords
        : DataRecords
    {
        public bool SkipSummaryLine { get; }

        public WeatherRecords(
            bool skipSummaryLine)
            : base(DataFormat.AllFields)
        {
            this.SkipSummaryLine = skipSummaryLine;
        }

        protected override bool ShouldParseLine(
            int lineNumber,
            string line)
        {
            if (lineNumber == 0
                || lineNumber == 1)
                return false; //skip first two header lines

            if (SkipSummaryLine
                && IsSummaryLine(lineNumber, line))
                return false;

            return base.ShouldParseLine(lineNumber, line);
        }

        private bool IsSummaryLine(
            int lineNumber,
            string line)
        {
            if (line is null)
                return false;

            if (lineNumber < 29)
                return false;// it's not the last line

            int minlen =
                DataFormat.Day.StartIndex +
                DataFormat.Day.Length;

            if (line.Length < minlen)
                return false; //too short

            var firstField = line.Substring(
                DataFormat.Day.StartIndex,
                DataFormat.Day.Length).Trim();

            return DayFieldInfo.MonthSummaryContents.Equals(firstField);
        }


    }
}