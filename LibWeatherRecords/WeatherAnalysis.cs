﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;
using System.Text;
using LibRecords;

namespace LibWeatherRecords
{
    public class WeatherAnalysis
        : Analysis<WeatherRecords, decimal>
    {
        public WeatherAnalysis(
            IFrontend frontend,
            bool skipSummaryLine)
            : base(
                  new WeatherRecords(skipSummaryLine),
                  frontend,
                  Reduce)
        {
        }

        private static AnalysisResult<decimal> Reduce(
            AnalysisResult<decimal> accumulator,
            Record current)
        {
            var minT = current.GetField(
                DataFormat.MinTemp);

            var maxT = current.GetField(
                DataFormat.MaxTemp);

            if (minT is null || maxT is null)
                return accumulator; //no data: skip current record;

            var diff = maxT.Value.Value - minT.Value.Value;

            var day = current.GetField(
                DataFormat.Day)?.Value.ToString();

            if (accumulator is null
                || accumulator.ResultValue > diff)
            {
                return new AnalysisResult<decimal>()
                {
                    ResultValue = diff,
                    RecordIndex = current.Index,
                    RecordName = $"Day {day}",
                    ResultName = $"Temp. variation"
                };
            }

            return accumulator;
        }
    }
}
