/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using LibRecords;

namespace LibWeatherRecords
{
    public class DayFieldInfo
        : FieldInfo<int>
    {
        public static string MonthSummaryContents { get; } = "mo";

        public static int MontSummaryIntValue { get; } = int.MinValue;

        public DayFieldInfo(
            string name,
            int startIdx,
            int endIdx,
            bool isOptional)
            : base(
                name,
                startIdx,
                endIdx,
                isOptional,
                (string v, out int i) =>
                {
                    v = v?.Trim();
                    if (MonthSummaryContents.Equals(v))
                    {
                        i = MontSummaryIntValue;
                        return true;
                    }

                    return int.TryParse(v, out i);
                })
        {
        }
    }
}