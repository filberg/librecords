/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */



using System;
using System.Collections.Generic;
using System.IO;

using LibTextify;

namespace Textify
{
    class Program
    {
        private static string OutFile { get; }
            = @"%HOMEDRIVE%%HOMEPATH%\LibTextify.out.txt";

        private static string SourceDir { get; }
            = @"..\..\..\..";


        private static Dictionary<string, bool> Excludes
            = new Dictionary<string, bool>()
            {
                { "obj", true},
                { "bin", true },
                {".git", true },
                { ".vs", true},
                { ".gitignore", true},
                { "specs", true}
            };

        static void Main(string[] args)
        {
            Textity();
        }


        static void Untextify()
        {
            var srcdir = new DirectoryInfo(SourceDir);
            var txf = new Textifier(srcdir);
            
            var outpath =
                Environment.ExpandEnvironmentVariables(OutFile);

            try
            {
                txf.Untextify(outpath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Environment.Exit(1);
            }

            Console.WriteLine(
                $"All files unpacked into {srcdir.FullName}");
        }

        static void Textity()
        {
            var srcdir = new DirectoryInfo(SourceDir);
            var txf = new Textifier(
                srcdir,
                (fd) =>
                {
                    if (Excludes.TryGetValue(fd.Name, out bool exclude))
                        return exclude;
                    else
                        return false;
                });
            var outpath =
                Environment.ExpandEnvironmentVariables(OutFile);

            try
            {
                txf.Textify(outpath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Environment.Exit(1);
            }

            Console.WriteLine(
                $"All files merged into {outpath}");
        }
    }
}

