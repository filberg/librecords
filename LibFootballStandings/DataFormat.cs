﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using LibRecords;

namespace LibFootballStandings
{
    public static class DataFormat
    {
        public static StringFieldInfo Standing { get; }
            = new StringFieldInfo("Standing", 0, 5, false);

        public static StringFieldInfo TeamName { get; }
            = new StringFieldInfo("Team", 7, 22, false);

        public static IntFieldInfo Played { get; }
            = new IntFieldInfo("P", 23, 24, false);

        public static IntFieldInfo Wins { get; }
            = new IntFieldInfo("W", 25, 30, false);

        public static IntFieldInfo Losses { get; }
            = new IntFieldInfo("L", 31, 34, false);

        public static IntFieldInfo Draws { get; }
            = new IntFieldInfo("D", 35, 38, false);

        public static IntFieldInfo GoalsFor { get; }
            = new IntFieldInfo("F", 39, 44, false);

        public static IntFieldInfo GoalsAgainst { get; }
            = new IntFieldInfo("A", 48, 51, false);

        public static IntFieldInfo Points { get; }
            = new IntFieldInfo("P", 52, 57, false);

        public static FieldInfo[] AllFields { get; }
            = new FieldInfo[]
            {
                Standing,
                TeamName,
                Played,
                Wins,
                Losses,
                Draws,
                GoalsFor,
                GoalsAgainst,
                Points
            };
    }
}
