﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibFootballStandings;
using LibRecords;

namespace LibFootballStandings
{
    public class FooballAnalysis
        : Analysis<FootballRecords, int>
    {
        public FooballAnalysis(
            IFrontend frontend,
            bool allowSeparatorLine)
            : base(
                  new FootballRecords(allowSeparatorLine),
                  frontend,
                  Reduce)
        {
        }

        private static AnalysisResult<int> Reduce(
            AnalysisResult<int> accumulator,
            Record current)
        {
            var gFor = current.GetField(
                DataFormat.GoalsFor);

            var gAgainst = current.GetField(
                DataFormat.GoalsAgainst);

            if (gFor is null || gAgainst is null)
                return accumulator; //no data: skip current record;

            int diff = Math.Abs(gFor.Value - gAgainst.Value);

            if (accumulator is null
                || diff < accumulator.ResultValue)
            {
                return new AnalysisResult<int>()
                {
                    RecordIndex = current.Index,
                    RecordName = current.GetField(DataFormat.TeamName).Value,
                    ResultName = "Goal difference",
                    ResultValue = diff
                };
            }

            //keep the current status
            return accumulator;
        }
    }
}
