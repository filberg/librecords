﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibRecords
{
    public class Analysis<TRecords, TResult>
        where TRecords : DataRecords
    {
        public TRecords Records { get; }

        public IFrontend FrontEnd { get; }

        private DataRecords.ReduceFunc<AnalysisResult<TResult>> Reducer;

        public Analysis(
            TRecords records,
            IFrontend frontend,
            DataRecords.ReduceFunc<AnalysisResult<TResult>> reduceFn)
        {
            this.FrontEnd = frontend
                ?? throw new ArgumentNullException(
                    nameof(frontend));

            this.Reducer = reduceFn
                ?? throw new ArgumentNullException(
                    nameof(reduceFn));

            this.Records = records
                ?? throw new ArgumentNullException(
                    nameof(records));
        }

        public AnalysisResult<TResult> Run()
        {
            try
            {
                var f = FrontEnd.GetRecordLines();
                if (f is null)
                    return null;

                var result = Records.Import(
                    f,
                    OnException,
                    OnError,
                    OnInfo);

                if (result.Result != ResultCode.OK)
                    return null;

                return this.Records.Reduce(
                    Reducer,
                    null);
            }
            catch(Exception e)
            {
                OnException(
                    new ImportEvent(true, -1, -1, e.Message, e));

                return null;
            }
        }

        protected virtual void OnError(ImportEvent e)
        {
            FrontEnd.Log(e.ToLogString());
            if (!string.IsNullOrEmpty(e.Message))
                FrontEnd.ShowError(e.Message);
        }

        protected virtual void OnInfo(ImportEvent e)
        {
            FrontEnd.Log(e.ToLogString());
        }

        protected virtual void OnException(ImportEvent e)
        {
            OnError(e);
            FrontEnd.ShowError(e.Exception?.ToString());
        }
    }
}
