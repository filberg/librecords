/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */


namespace LibRecords
{
    static class Strings
    {
        public static readonly string ErrGeneric =
            "Generic error.";

        public static readonly string ErrFileAccess =
             "Cannot read input file. " + 
             "Please verify that the file is accessible, " + 
             "that it's not opened by another process " + 
             "and that the current user has the necessary permissions.";

        public static readonly string ErrLineTooShort =
            "Line is too short.";

        public static readonly string ErrInvalidDataFormat =
            "Invalid data format";

        public static readonly string MsgBeginImport =
            "Starting file import..";

        public static readonly string MsgLineSkipped =
            "Line skipped. The current settings prevented from parsing the line.";
    }
}