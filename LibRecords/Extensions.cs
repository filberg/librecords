﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Text;
using LibRecords;

namespace LibRecords
{
    public static class Extensions
    {
        public static string ToLogString(this ImportEvent e)
        {
            var sb = new StringBuilder();

            sb.Append(e.Timestamp.ToLongDateString())
                .Append(" ");


            if (e.IsException)
                sb.Append("EXC: ");
            else if (e.IsError)
                sb.Append("ERR: ");
            else
                sb.Append("INF: ");

            if (e.Line >= 0)
            {
                sb.Append("line ").Append(e.Line);
                if (e.Column >= 0)
                    sb.Append(", column ").Append(e.Column);
                sb.Append(" ");
            }

            sb.Append(e.Message);

            return sb.ToString();
        }
    }
}
