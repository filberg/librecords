/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;

namespace LibRecords
{
    public enum ResultCode
    {
        OK = 0,
        Fail = 1,
        LineTooShort = 2,
        WrongDataFormat = 3
    }

    public class ImportEvent
    {
        public bool IsError { get; }

        public bool IsException
            => Exception != null;

        public int Line { get; }

        public int Column { get; }

        public string Message { get; }

        public Exception Exception { get; }

        public DateTime Timestamp { get; }

        public ImportEvent(
            bool isError,
            int line,
            int col,
            string message,
            Exception ex)
        {
            this.Timestamp = DateTime.Now;

            this.IsError = isError;
            this.Line = line;
            this.Column = col;
            this.Message = message;
            this.Exception = ex;
            
            if (ex != null)
                this.IsError = true;
        }
    }
}