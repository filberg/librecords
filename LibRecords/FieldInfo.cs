/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;

namespace LibRecords
{
    public abstract class FieldInfo
    {
        public int StartIndex { get; }

        public int EndIndex { get; }

        public abstract Type ValueType { get; }

        public string Name { get; }

        public int Length
            => (EndIndex - StartIndex) + 1;

        public bool IsOptional { get; }

        public FieldInfo(
            string name,
            int startIdx,
            int endIdx,
            bool isOptional)
        {
            this.Name = name
                ?? throw new ArgumentNullException(
                    nameof(name));

            if (startIdx < 0)
                throw new IndexOutOfRangeException(
                    $"{nameof(startIdx)} cannot be less than zero");

            if (endIdx < 0)
                throw new IndexOutOfRangeException(
                    $"{nameof(endIdx)} cannot be less than zero");

            if (startIdx >= endIdx)
                throw new IndexOutOfRangeException(
                    $"{nameof(endIdx)} must be greater than {nameof(startIdx)}");

            this.StartIndex = startIdx;
            this.EndIndex = endIdx;
            this.IsOptional = isOptional;
        }

        public abstract ParseResult<Field> ParseFromLine(string line);

        public abstract Field GetNullField();
    }

    public class FieldInfo<T>
        : FieldInfo
    {
        /// <summary>
        /// A function to to parse the given <see cref="value"/> into the corresponding <see cref="result"/>
        /// </summary>
        /// <param name="value">A string representation of the value to be parsed</param>
        /// <param name="result">The parsed result</param>
        /// <returns>True if the parsing succeeds, false otherwise</returns>
        public delegate bool ParsingFunc(string value, out T result);

        public override Type ValueType
            => typeof(T);

        private ParsingFunc Parser { get; }

        public FieldInfo(
            string name,
            int startIdx,
            int endIdx,
            bool isOptional,
            ParsingFunc parser)
            : base(name, startIdx, endIdx, isOptional)
        {
            this.Parser = parser
                ?? throw new ArgumentNullException(
                    nameof(parser));
        }

        public bool TryParse(string rawValue, out Field<T> result)
        {
            if (Parser(rawValue, out T res))
            {
                result = new Field<T>(this, res, true);
                return true;
            }

            result = null;
            return false;
        }

        public override ParseResult<Field> ParseFromLine(string line)
        {
            if (line.Length <= EndIndex)
            {
                // line ends before the required field
                if (this.IsOptional)
                    return ParseRes.Ok(
                        GetNullField());
                else
                    return ParseRes.Fail<Field>(
                        ResultCode.LineTooShort,
                        line.Length);
            }

            var rawVal = line.Substring(
                StartIndex,
                Length).Trim();

            if (IsOptional
                && rawVal.Length == 0)
                return ParseRes.Ok(GetNullField());

            if (TryParse(rawVal, out Field<T> f))
                return ParseRes.Ok((Field)f);

            return ParseRes.Fail<Field>(
                ResultCode.WrongDataFormat,
                StartIndex);
        }

        public Field<T> GetNullFieldTyped()
        {
            return new Field<T>(
                this,
                default(T),
                false);
        }

        public override Field GetNullField()
            => GetNullFieldTyped();
    }
}