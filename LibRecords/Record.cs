/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;

namespace LibRecords
{
    public class Record
    {
        public int Index { get; }

        private Dictionary<string, Field> _fieldMap;

        public IEnumerable<Field> Fields
            => _fieldMap.Values;

        public int FieldCount
            => _fieldMap.Count;

        internal Record(
            int index,
            Dictionary<string, Field> fields)
        {
            this.Index = index;
            this._fieldMap = fields
                ?? throw new ArgumentNullException(
                    nameof(fields));
        }

        public Field<T> GetField<T>(FieldInfo<T> fieldInfo)
        {
            if (fieldInfo is null)
                throw new ArgumentNullException(
                    nameof(fieldInfo));

            if (_fieldMap.TryGetValue(fieldInfo.Name, out Field f))
                return (Field<T>)f;
            else
                return null;
        }
    }
}