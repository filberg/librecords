﻿/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LibRecords
{
    public class DataRecords
    {
        public delegate T ReduceFunc<T>(
            T accumulator,
            Record currentElement);

        public FieldInfo[] RecordStructure { get; }

        public int RecordCount { get; private set; }

        public IEnumerable<Record> Records => _records;

        private List<Record> _records;

        public DataRecords(
            FieldInfo[] recordStructure)
        {
            this.RecordStructure = recordStructure
                ?? throw new ArgumentNullException(
                    nameof(recordStructure));

            if (recordStructure.Length == 0)
                throw new ArgumentException(
                    $"{nameof(recordStructure)} " +
                    "must contain at least 1 element");

            this._records = new List<Record>();
            this.RecordCount = 0;
        }

        /// <summary>
        /// When overridden in derived classes,
        /// provides a method to avoid parsing a given line.
        /// If not overridden, always returns true.
        /// If this method returns false, the current line is ignored.
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns>True if the current line should be parsed, false otherwise.</returns>
        protected virtual bool ShouldParseLine(
            int lineNumber,
            string line)
        {
            return true;
        }

        public ImportReport Import(
            IEnumerable<string> recordLines,
            ImportEventHandler onException = null,
            ImportEventHandler onError = null,
            ImportEventHandler onInfo = null)
        {
            if (recordLines is null)
                throw new ArgumentNullException(
                    nameof(recordLines));

            var ir = new ImportReport(
                onError,
                onInfo,
                onException);

            int currIdx = -1;

            ir.LogInfo(
                Strings.MsgBeginImport);
            try
            {
                foreach (var line in recordLines)
                {
                    if (!ImportLine(++currIdx, line, ir))
                        return ir.Fail();
                }
            }
            catch (Exception e)
            {
                ir.LogEx(e, currIdx);
                return ir.Fail();
            }

            return ir.Succeed();
        }

        private bool ImportLine(
            int lineNumber,
            string line,
            ImportReport ir)
        {
            if (!ShouldParseLine(lineNumber, line))
            {
                ir.LogInfo(
                    Strings.MsgLineSkipped,
                    lineNumber,
                    0);

                return true;
            }

            var f = ParseNextRecord(line);

            switch (f.ResultCode)
            {
                case ResultCode.OK:
                    _records.Add(f.Result);
                    return true;

                case ResultCode.LineTooShort:
                    ir.LogErr(
                        Strings.ErrLineTooShort,
                        lineNumber,
                        line.Length - 1);
                    return false;

                case ResultCode.WrongDataFormat:
                    ir.LogErr(
                        Strings.ErrInvalidDataFormat,
                        lineNumber,
                        f.ColumnNumber);
                    return false;

                case ResultCode.Fail:
                default:
                    ir.LogErr(
                        Strings.ErrGeneric,
                        lineNumber,
                        f.ColumnNumber);
                    return false;
            }
        }

        public ParseResult<Record> ParseNextRecord(string line)
        {
            var fields = new Dictionary<string, Field>();

            foreach (var info in RecordStructure)
            {
                var f = info.ParseFromLine(line);

                if (f.ResultCode == ResultCode.OK)
                    fields[info.Name] = f.Result;
                else
                    return f.Translate<Record>();
            }

            var recnum = _records.Count;
            var r = new Record(
                recnum,
                fields);

            return ParseRes.Ok(r);
        }

        public T Reduce<T>(
            ReduceFunc<T> reduceFn,
            T firstElement)
        {
            var accum = firstElement;
            foreach (var rec in Records)
                accum = reduceFn(accum, rec);

            return accum;
        }
    }
}
