/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;

namespace LibRecords
{
    public abstract class Field
    {
        public FieldInfo RawInfo { get; }

        public object RawValue { get; }

        public virtual bool HasValue
            => RawValue != null;

        public Field(
            FieldInfo fieldInfo,
            object value)
        {
            this.RawInfo = fieldInfo
                ?? throw new ArgumentNullException(
                    nameof(fieldInfo));

            this.RawValue = value;
        }

    }

    public class Field<T>
        : Field
    {
        public FieldInfo<T> Info { get; }

        public T Value => (T)RawValue;

        private bool _hasValue;
        public override bool HasValue
            => _hasValue;

        public Field(
            FieldInfo<T> fieldInfo,
            T value,
            bool hasValue)
            : base(fieldInfo, value)
        {
            this._hasValue = hasValue;
        }
    }
}