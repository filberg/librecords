/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace LibRecords
{
    public class DecimalFieldInfo
        : FieldInfo<decimal>
    {
        public DecimalFieldInfo(
            string name,
            int startIdx,
            int endIdx,
            bool isOptional)
            : base(name,
                startIdx,
                endIdx,
                isOptional,
                (string v, out decimal r)
                    => decimal.TryParse(v?.Trim(), out r))
        {
        }
    }

    public class StringFieldInfo
        : FieldInfo<string>
    {
        public StringFieldInfo(
            string name,
            int startIdx,
            int endIdx,
            bool isOptional)
                : base(name,
                    startIdx,
                    endIdx,
                    isOptional,
                    (string v, out string r) =>
                    {
                        r = v;
                        return true;
                    })
        {
        }
    }

    public class IntFieldInfo
        : FieldInfo<int>
    {
        public IntFieldInfo(
            string name,
            int startIdx,
            int endIdx,
            bool isOptional) 
            : base(name,
                startIdx,
                endIdx,
                isOptional,
                (string v, out int i) 
                    => int.TryParse(v?.Trim(), out i))
        {
        }
    }
}