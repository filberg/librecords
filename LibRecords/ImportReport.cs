/*
 *  Copyright (c) 2021, Filippo Bergamo
 *
 *  This file is part of filberg's LibRecords.
 *
 *  LibRecords is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LibRecords is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LibRecords. If not, see <https://www.gnu.org/licenses/>.
 *
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace LibRecords
{
    public delegate void ImportEventHandler(ImportEvent e);

    public class ImportReport
    {
        public ResultCode Result { get; internal set; }

        public ImportEvent[] Events
            => _events.ToArray();

        public ImportEvent[] Errors
            => _events.Where(
                (e) => e.IsError
                ).ToArray();

        public ImportEvent[] Infos
            => _events.Where(
                (e) => !e.IsError
                ).ToArray();

        private List<ImportEvent> _events;

        public ImportEventHandler OnError { get; }

        public ImportEventHandler OnInfo { get; }

        public ImportEventHandler OnException { get; }

        internal ImportReport(
            ImportEventHandler onError,
            ImportEventHandler onInfo,
            ImportEventHandler onException)
        {
            _events = new List<ImportEvent>();
            this.OnError = onError;
            this.OnInfo = onInfo;
            this.OnException = onException;
        }

        internal void LogInfo(
            string message)
            => LogInfo(message, -1, -1);

        internal void LogInfo(
            string message,
            int lineNumber,
            int columnNumber)
        {
            Log(
                new ImportEvent(
                    false,
                    lineNumber,
                    columnNumber,
                    message,
                    null));
        }

        internal void LogErr(
            string message)
            => LogErr(message, -1, -1);

        internal void LogErr(
            string message,
            int lineNumber,
            int columnNumber)
        {
            Log(
                new ImportEvent(
                    true,
                    lineNumber,
                    columnNumber,
                    message,
                    null));
        }

        internal void LogEx(
            Exception ex)
            => LogEx(ex, -1);

        internal void LogEx(
            Exception ex,
            int lineNumber)
        {
            Log(
                new ImportEvent(
                    true,
                    lineNumber,
                    -1,
                    ex?.ToString(),
                    ex));
        }

        private void Log(ImportEvent e)
        {
            this._events.Add(e);

            if (e.IsError)
            {
                if (e.Exception != null)
                    this.OnException?.Invoke(e);
                else
                    this.OnError?.Invoke(e);
            }
            else
            {
                this.OnInfo?.Invoke(e);
            }
        }

        public ImportReport Fail()
            => Complete(ResultCode.Fail);

        public ImportReport Succeed()
            => Complete(ResultCode.OK);

        public ImportReport Complete(ResultCode resCode)
        {
            this.Result = resCode;
            return this;
        }
    }
}